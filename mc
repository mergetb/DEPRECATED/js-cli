#!/usr/bin/env node

/*=============================================================================
 * merge command line client
 *===========================================================================*/

// includes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
var program = require('commander');
var request = require('request');
var cli = require('./cli-common.js');
var os = require('os');
var fs = require('fs');
var rlz = require('./realize.js');
var pp = require('./push-pull.js');

// cli setup ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
program
  .version('0.1.0')
  .command('init <portal>')
  .description('set the portal address')
  .action(init);

program
  .command('health')
  .description('get portal health')
  .action(health);

program
  .command('get <name>', 'query portal');

program
  .command('put <name>', 'put things');

program
  .command('del <name>', 'delete things');

program
  .command('show <name>', 'show things');

program
  .command('realize <project> <experiment> <realization-name> <modelfile>')
  .description('realize an experiment')
  .action(rlz.realize);

program
  .command('accept <project> <experiment> <realization>')
  .description('accept a realization')
  .action(rlz.accept);

program
  .command('reject <project> <experiment> <realization>')
  .description('reject a realization')
  .action(rlz.reject);

program
  .command('push <project> <experiment> <filename>')
  .description('push experiment topology source')
  .action(pp.push);

program
  .command('pull <project> <experiment> <hash>')
  .description('pull experiment source by hash')
  .action(pp.pull);

program
  .command('fetch <project> <experiment>')
  .description('fetch a list of experiment sources')
  .action(pp.fetch);


program.parse(process.argv);

// handlers ==================================================================~

function init(url) {
  var merge_dir = os.homedir()+"/.merge";
  if(!fs.existsSync(merge_dir)) {
    fs.mkdirSync(merge_dir);
  }
  var url_path = merge_dir+"/portal";
  fs.writeFileSync(url_path, url);
}

function health() {
  cli.withLogin(function() {

    request.get(cli.url(program)+'/health', function(error, response, body) {
      if(error != null) {
        console.log('portal api call failed: ', error);
      }
      else if(response.statusCode != 200) {
        console.log('bad status: ', response.statusCode);
      }
      else {
        console.log(body);
      }
    });

  });
}

