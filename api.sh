#!/usr/bin/env bash

OP=get
BODY=
POST_HEADER=
DATA=
# Grr it's an auth bear
AUTHBEAR=${AUTHBEAR:-~/src/auth0-curl/auth0-curl -c $(pwd)/.passwd}
PORTALFILE=${PORTALFILE:-~/.merge/portal}

while getopts o:a:b:p:hH opt; do
	case $opt in
        o) OP=$OPTARG
            ;;
        b) BODY=$OPTARG
            ;;
        a) AUTHBEAR=$OPTARG
            ;;
        p) PORTALFILE=$OPTARG
            ;;
        h|H) echo $(basename $0) \[-o \get\|post\|put\|delete] \[-b body.json\] \[-p portalfile\] \[-a authscript\] URL
            echo get is the default operation. If post or put, -b path/to/file is required.
            echo You can also pass in the path \(plus args\) to the authorization bearer script via \'-a path\'
            echo You can also pass the path the portal file via \'-p path\'
            exit 1
            ;;
    esac
done

shift $((OPTIND-1))

TOKEN=$($AUTHBEAR)
PORTAL=$(cat $PORTALFILE)

if [[ $OP == delete ]]; then
    curl \
        -s -k -w "%{response_code}" \
        -X DELETE \
        -H "authorization: bearer $TOKEN " \
        https://$PORTAL/$1
    echo
elif [[ $OP == post || $OP == put ]]; then
    curl \
        -s -k -w "%{response_code}" \
        -X ${OP} \
        -T ${BODY} \
        -H "Content-Type: application/json" \
        -H "authorization: bearer $TOKEN" \
        https://$PORTAL/$1
    echo
elif [[ $OP =~ get ]]; then 
    curl \
        -s \
        -w "%{response_code}" \
        -k \
        ${BODY} \
        ${POST_HEADER} \
        -H "authorization: bearer $TOKEN" \
        https://$PORTAL/$1| jq
else
    echo bad operation
    exit 1
fi

exit $?
