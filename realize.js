var fs = require('fs');
var cli = require('./cli-common.js');

function realize(proj, xp, name, modelfile) {

  let model = "";
  try {
    model = fs.readFileSync(modelfile);
  }
  catch (e) {
    console.log("model", modelfile, "not found")
    process.exit(1);
  }


  let path = "/projects/"+proj+"/experiments/"+xp+"/realizations/"+name;
  let body = {
    name: name,
    model: ""+model,
  };

  cli.putStuff(path, body, (code, response) => {
    cli.pretty({
      code: code,
      response: response
    })
  });



}

function get(proj, xp) {
  let path = "/projects/"+proj+"/experiments/"+xp+"/realizations";
  cli.getStuff(path, function(stuff) {
    cli.pretty(stuff);
  });
}

function show(proj, xp, name) {
  let path = "/projects/"+proj+"/experiments/"+xp+"/realizations/"+name;
  cli.getStuff(path, function(stuff) {
    cli.pretty(stuff);
  });
}

function accept(proj, xp, name) {
  let path = "/projects/"+proj+"/experiments/"+xp+"/realizations/"+name+"/act";
  let data = {
    action: 'accept'
  };
  cli.postStuff(path, data, function(code, stuff) {
    console.log("code:", code)
    cli.pretty(stuff);
  });
}

function reject(proj, xp, name) {
  let path = "/projects/"+proj+"/experiments/"+xp+"/realizations/"+name+"/act";
  let data = {
    action: 'reject'
  }
  cli.postStuff(path, data, function(code, stuff) {
    console.log("code:", code)
    cli.pretty(stuff);
  });
}

function del(proj, xp, name) {
  let path = "/projects/"+proj+"/experiments/"+xp+"/realizations/"+name;
  cli.delStuff(path, function(response) {
    cli.pretty(response);
  });
}

module.exports = {
  realize,
  get,
  show,
  accept,
  reject,
  del
}
