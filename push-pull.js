var cli = require('./cli-common.js');
var fs = require('fs');

function push(proj, xp, file) {

  let model = "";
  try {
    model = fs.readFileSync(file);
  }
  catch (e) {
    console.log("model", file, "not found")
    process.exit(1);
  }

  let path = '/projects/'+proj+'/experiments/'+xp+'/src';
  let body = { src: ""+model };


  cli.postStuff(path, body, function(code, response) {
    cli.pretty({
      code: code,
      response: response
    });
  })

}

function pull(proj, xp, hash) {

  cli.getStuff('/projects/'+proj+'/experiments/'+xp+'/src/'+hash, function(data) {
    cli.pretty({
      pushDate: new Date(data.pushDate).toLocaleString(),
      who: data.who,
      src: 'written to ./src.py'
    });
    fs.writeFileSync('src.py', data.src);
  });

}

function fetch(proj, xp) {

  cli.getStuff('/projects/'+proj+'/experiments/'+xp+'/src', function(hashes) {
    hashes.forEach(x => console.log(x));
  });

}

module.exports = { push, pull, fetch }

